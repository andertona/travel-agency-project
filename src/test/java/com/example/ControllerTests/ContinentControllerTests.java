package com.example.ControllerTests;

import com.example.mvc.controllers.ContinentController;
import com.example.mvc.entities.Continent;
import com.example.mvc.services.ContinentService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

    @RunWith(SpringRunner.class)
    @SpringBootTest(classes = ContinentController.class)
    public class ContinentControllerTests {

        @MockBean
        ContinentService continentService;

        ObjectMapper mapper = new ObjectMapper();

        @Autowired
        private MockMvc mockMvc;

        @Test
        public void it_should_return_created_continent() throws Exception {
            Continent request = new Continent();
            request.setName("test continent");

            Continent continent = new Continent();
            continent.setName(request.getName());

            when(continentService.save(request)).thenReturn(continent);
            mockMvc.perform(post("/continents")
                    .content(mapper.writeValueAsString(request))
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.name").value(request.getName()));


        }
}
