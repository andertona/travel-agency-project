package com.example.ControllerTests;

import com.example.mvc.controllers.TripController;
import com.example.mvc.entities.Trip;
import com.example.mvc.services.TripService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

    @RunWith(SpringRunner.class)
    @SpringBootTest(classes = TripController.class)
    public class TripControllerTests {

        @MockBean
        TripService tripService;

        ObjectMapper mapper = new ObjectMapper();

        @Autowired
        private MockMvc mockMvc;

        @Test
        public void it_should_return_created_trip() throws Exception {
            Trip request = new Trip();
            request.setType("test trip");

            Trip trip = new Trip();
            trip.setType(request.getType());

            when(tripService.save(request)).thenReturn(trip);
            mockMvc.perform(post("/trips")
                    .content(mapper.writeValueAsString(request))
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.name").value(request.getType()));


        }
}
