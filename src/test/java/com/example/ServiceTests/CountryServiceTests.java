package com.example.ServiceTests;

import com.example.mvc.entities.Country;
import com.example.mvc.repositories.CountryRepository;
import com.example.mvc.services.CountryService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CountryServiceTests {

    @Mock
    CountryRepository countryRepository;

    @InjectMocks
    CountryService countryService;

    @Test
    public void testCountrySave() {

        Country countryRequest = new Country();
        countryRequest.setName("test country");

        Country countryResponse = new Country();
        countryResponse.setId(2L);
        countryResponse.setName("test country");

        when(countryRepository.save(countryRequest)).thenReturn(countryResponse);

        Country created = countryService.save(countryRequest);

        assertTrue(created.getName().equals(countryRequest.getName()));
        assertNotNull(created.getId());

    }

    @Test
    public void testFindAll() {
        List<Country> countries = List.of(new Country(), new Country(), new Country());

        when(countryRepository.findAll()).thenReturn(countries);

        List<Country> created = countryService.findAllCountries();

        assertNotNull(created);
        assertTrue(!created.isEmpty());
        assertTrue(created.size() == 3);

    }
}