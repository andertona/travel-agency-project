package com.example.ServiceTests;

import com.example.mvc.entities.City;
import com.example.mvc.repositories.CityRepository;
import com.example.mvc.services.CityService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;


    @RunWith(MockitoJUnitRunner.class)
    public class CityServiceTests {

        @Mock
        CityRepository cityRepository;

        @InjectMocks
        CityService cityService;

        @Test
        public void testCitySave() {

            City cityRequest = new City();
            cityRequest.setName("test client");

            City cityResponse = new City();
            cityResponse.setId(2L);
            cityResponse.setName("test client");

            when(cityRepository.save(cityRequest)).thenReturn(cityResponse);

            City created = cityService.save(cityRequest);

            assertTrue(created.getName().equals(cityRequest.getName()));
            assertNotNull(created.getId());

        }

        @Test
        public void testFindAll() {
            List<City> cities = List.of(new City(), new City(), new City());

            when(cityRepository.findAll()).thenReturn(cities);

            List<City> created = cityService.findAllCities();

            assertNotNull(created);
            assertTrue(!created.isEmpty());
            assertTrue(created.size() == 3);

        }
    }
