package com.example.ServiceTests;

import com.example.mvc.entities.Purchase;
import com.example.mvc.repositories.PurchaseRepository;
import com.example.mvc.services.PurchaseService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PurchaseServiceTests {

    @Mock
    PurchaseRepository purchaseRepository;

    @InjectMocks
    PurchaseService purchaseService;

    @Test
    public void testPurchaseSave() {

        Purchase purchaseRequest = new Purchase();
        purchaseRequest.setName("test purchase");

        Purchase purchaseResponse = new Purchase();
        purchaseResponse.setId(2L);
        purchaseResponse.setName("test Purchase");

        when(purchaseRepository.save(purchaseRequest)).thenReturn(purchaseResponse);

        Purchase created = purchaseService.save(purchaseRequest);

        assertTrue(created.getName().equals(purchaseRequest.getName()));
        assertNotNull(created.getId());

    }

    @Test
    public void testFindAll() {
        List<Purchase> purchases = List.of(new Purchase(), new Purchase(), new Purchase());

        when(purchaseRepository.findAll()).thenReturn(purchases);

        List<Purchase> created = purchaseService.findAllPurchases();

        assertNotNull(created);
        assertTrue(!created.isEmpty());
        assertTrue(created.size() == 3);

    }
}
