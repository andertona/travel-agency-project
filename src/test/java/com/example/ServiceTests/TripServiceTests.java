package com.example.ServiceTests;


import com.example.mvc.entities.Trip;
import com.example.mvc.repositories.TripRepository;
import com.example.mvc.services.TripService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class TripServiceTests {

    @Mock
    TripRepository tripRepository;

    @InjectMocks
    TripService tripService;

    @Test
    public void testTripSave() {

        Trip tripRequest = new Trip();
        tripRequest.setType("test trip");

        Trip tripResponse = new Trip();
        tripResponse.setId(2L);
        tripResponse.setType("test trip");

        when(tripRepository.save(tripRequest)).thenReturn(tripResponse);

        Trip created = tripService.save(tripRequest);

        assertTrue(created.getType().equals(tripRequest.getType()));
        assertNotNull(created.getId());

    }

    @Test
    public void testFindAll() {
        List<Trip> trips = List.of(new Trip(), new Trip(), new Trip());

        when(tripRepository.findAll()).thenReturn(trips);

        List<Trip> created = tripService.findAllTrips();

        assertNotNull(created);
        assertTrue(!created.isEmpty());
        assertTrue(created.size() == 3);

    }
}
