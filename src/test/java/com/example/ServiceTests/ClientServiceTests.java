package com.example.ServiceTests;

import com.example.mvc.entities.Client;
import com.example.mvc.repositories.ClientRepository;
import com.example.mvc.services.ClientService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ClientServiceTests {

    @Mock
    ClientRepository clientRepository;

    @InjectMocks
    ClientService clientService;

    @Test
    public void when_save_client_it_should_return_client() {
        Client client = new Client();
        client.setNameSurname("test client");

        Client clientResponse = new Client();
        clientResponse.setNameSurname("test client");

        when(clientRepository.save(client)).thenReturn(clientResponse);

        Client created = clientService.save(client);

        assertTrue(created.getNameSurname().equals(client.getNameSurname()));

    }
}
