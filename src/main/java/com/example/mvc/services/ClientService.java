package com.example.mvc.services;

import com.example.mvc.entities.Client;
import com.example.mvc.entities.ID;
import com.example.mvc.repositories.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import javax.validation.Valid;
import java.util.List;

@Service
@Transactional

public class ClientService {
    @Autowired
    private ClientRepository clientRepository;

    public List<Client> findAllClients() {
        return clientRepository.findAll();
    }

    public Client findById(Long id) {
        return clientRepository.findById(id).get();
    }


    public Client save(Client client) {
        return clientRepository.save(client);
    }

    public void delete(Client client) {
        clientRepository.delete(client);
    }

    public void deleteById(@Valid ID client) {
        clientRepository.deleteById(client.getId());
    }
}