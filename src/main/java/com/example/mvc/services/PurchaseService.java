package com.example.mvc.services;

import com.example.mvc.entities.Purchase;
import com.example.mvc.repositories.PurchaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class PurchaseService {

    @Autowired
    private PurchaseRepository purchaseRepository;


    public List<Purchase> findAll() {
        return purchaseRepository.findAll();
    }

    public Purchase findById(Long id) {
        return purchaseRepository.findById(id).get();
    }

    public Purchase save(Purchase purchase) {
        return purchaseRepository.save(purchase);
    }

    public void delete(Purchase purchase) {
        purchaseRepository.delete(purchase);
    }

    public void deleteById(Long purchase) {
        purchaseRepository.deleteById(purchase);
    }
}
