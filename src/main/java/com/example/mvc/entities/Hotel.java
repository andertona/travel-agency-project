package com.example.mvc.entities;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
@Data
public class Hotel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message = "Name cannot be null")
    private String name;

    @Min(value = 0)
    @Max(value = 7)
    private Integer qualityRank;

    @Size(min = 10, max = 200, message = "Description must be between 10 and 200 characters")
    private String description;

    @ManyToOne(fetch = FetchType.LAZY)
    private City city;

}
