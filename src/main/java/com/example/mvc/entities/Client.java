package com.example.mvc.entities;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import java.util.List;

@Entity
@Data
public class Client {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @NotBlank
    @Email(message = "Email should be valid")
    private String email;
    @NotBlank(message = "Name and Surname cannot be null")
    private String nameSurname;
    @Min(value = 8, message = "Phone number should not be less than 8")
    @Max(value = 15, message = "Phone number should not be greater than 15")
    private String phoneNumber;

     @OneToMany(mappedBy = "client", cascade = CascadeType.REMOVE, orphanRemoval = true)
     private List<Purchase> purchases;


}
