package com.example.mvc.entities;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
@Data
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message = "Country is mandatory")
    private String country;
    @NotBlank(message = "City is mandatory")
    private String city;
    @NotBlank(message = "Street name is mandatory")
    private String streetName;
    @NotBlank(message = "House number/name is mandatory")
    @Size(max = 16)
    private String houseNumber;
    @Size(max = 16)
    private String apartmentNumber;

    @ManyToOne(fetch = FetchType.LAZY)
    private User user;


}
