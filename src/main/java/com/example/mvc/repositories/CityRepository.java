package com.example.mvc.repositories;

import com.example.mvc.entities.City;
import com.example.mvc.entities.ID;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface CityRepository extends CrudRepository<City, Long> {
    List<City> findAll();
    Optional<City> findById(Long id);
    City save (City city1);
    void delete (City var);
    void deleteById(Long id);
    void deleteById(ID city);
}
