package com.example.mvc.repositories;

import com.example.mvc.entities.Purchase;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface PurchaseRepository extends CrudRepository<Purchase, Long> {

    List<Purchase> findAll();

    Optional<Purchase> findById(Long id);

    Purchase save (Purchase purchase);

    void delete(Purchase var1);

    void deleteById(Long id);

}
