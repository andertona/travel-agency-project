package com.example.mvc.repositories.examples;


import com.example.mvc.entities.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends PagingAndSortingRepository<User, Long> {
    List<User> findByName(String name);
    List<User> findAll();
    List<User> findByLogin(String login);

    @Query("SELECT id FROM User WHERE age > 18")
    List<User> findUsersOver18();

//    @Query("FROM Event WHERE status = :status AND TIME_TO_SEC(TIMEDIFF(:now, lastUpdateTs)) >= :duration")
//    List<User> findByStatusAndDuration(@Param("status") String status, @Param("duration") Integer duration, @Param("now") Integer now);
}
