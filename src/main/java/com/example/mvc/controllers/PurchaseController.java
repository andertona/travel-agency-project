package com.example.mvc.controllers;

import com.example.mvc.entities.ID;
import com.example.mvc.entities.Message;
import com.example.mvc.entities.Purchase;
import com.example.mvc.services.PurchaseService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/purchase")

public class PurchaseController {

    private final PurchaseService purchaseService;

    @GetMapping("/showAll")
    public ResponseEntity<List<Purchase>> findAll() {
        List<Purchase> purchase = purchaseService.findAll();
        return ResponseEntity.ok(purchase);
    }

    @GetMapping("/show/{id}")
    public ResponseEntity<Purchase> findById(@PathVariable Long id) {
        Purchase purchase = purchaseService.findById(id);
        return ResponseEntity.ok(purchase);
    }


    @PostMapping("/save")
    public ResponseEntity<Purchase> save(@Valid @RequestBody Purchase purchase){
        Purchase purchase1 = purchaseService.save(purchase);
        return ResponseEntity.ok(purchase1);
    }


    @DeleteMapping("/delete")
    public ResponseEntity<Message> delete(@Valid @RequestBody Purchase purchase){
        purchaseService.delete(purchase);
        return ResponseEntity.ok((new Message("Record deleted")));
    }

    @DeleteMapping("/deleteById")
    public ResponseEntity<Message> deleteById(@Valid @RequestBody ID purchase) {
        purchaseService.deleteById(purchase.getId());
        return ResponseEntity.ok((new Message("Record deleted")));

    }
}
