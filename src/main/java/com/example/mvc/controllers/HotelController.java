package com.example.mvc.controllers;


import com.example.mvc.entities.Hotel;
import com.example.mvc.entities.ID;
import com.example.mvc.entities.Message;
import com.example.mvc.services.HotelService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/hotel")

public class HotelController {

    private final HotelService hotelService;

    @GetMapping("/showAll")
    public ResponseEntity<List<Hotel>> findAll() {
        List<Hotel> hotels = hotelService.findAllHotels();
        return ResponseEntity.ok(hotels);
    }

    @GetMapping("/show/{id}")
    public ResponseEntity<Hotel> findById(@PathVariable Long id) {
       Hotel hotel = hotelService.findById(id);
        return ResponseEntity.ok(hotel);
    }

    @PostMapping("/save")
    public ResponseEntity<Hotel> save(@Valid @RequestBody Hotel hotel){
        Hotel hotel1 = hotelService.save(hotel);
        return ResponseEntity.ok(hotel1);
    }


    @DeleteMapping("/delete")
    public ResponseEntity<Message> delete(@Valid @RequestBody Hotel hotel){
        hotelService.delete(hotel);
        return ResponseEntity.ok((new Message("Record deleted")));
    }

    @DeleteMapping("/deleteById")
    public ResponseEntity<Message> deleteById(@Valid @RequestBody ID hotel) {
        hotelService.deleteById(hotel);
        return ResponseEntity.ok((new Message("Record deleted")));

    }
}